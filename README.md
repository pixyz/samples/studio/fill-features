# fill-features

Studio Plugin to take advantage of the *algo.listFeatures* API function. Get metadata information on models features. Fill features with 3D models.

![](images/fill_features.gif)


## Required Software

![](images/Logo-Puce_STUDIO_64x64.png)|
:-:|
PiXYZ Studio 2019|

## Set up

Clone the project in *C:/ProgramData/PiXYZStudio/plugins*. Restart Studio, the Plugin should appear in the PLUGINS tab.