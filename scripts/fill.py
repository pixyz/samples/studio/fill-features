import math
import information
try:
	from pxz import geom
	from pxz import scene
	from pxz import core
	from pxz import algo
	from pxz import cad
	from pxz import io
except:
	pass

def point_x(pt):
	return pt[0]
	
def point_y(pt):
	return pt[1]

def point_z(pt):
	return pt[2]
	
def point_add(pt1, pt2):
	return [point_x(pt1)+point_x(pt2), point_y(pt1)+point_y(pt2), point_z(pt1)+point_z(pt2)]

def point_sub(pt1, pt2):
	return [point_x(pt1)-point_x(pt2), point_y(pt1)-point_y(pt2), point_z(pt1)-point_z(pt2)]

def dot_product(pt1, pt2):
	return point_x(pt1)*point_x(pt2) + point_y(pt1)*point_y(pt2) + point_z(pt1)*point_z(pt2)

def cross_product(pt1, pt2):
	return [point_y(pt1)*point_z(pt2) - point_z(pt1)*point_y(pt2), point_z(pt1)*point_x(pt2) - point_x(pt1)*point_z(pt2), point_x(pt1)*point_y(pt2) - point_y(pt1)*point_x(pt2)]

def vector_length(v):
	return math.sqrt(dot_product(v,v))

def normalize(v):
	l = vector_length(v)
	return [point_x(v)/l, point_y(v)/l, point_z(v)/l]

def computeMatrixFromTransform(position, direction):
	x = cross_product([0,1,0], direction)
	if vector_length(x) < 0.001:
		x = cross_product(direction, [1,0,0])
		if vector_length(x) < 0.001:
			x = cross_product(direction, [0,0,1])
	x = normalize(x)
	y = normalize(cross_product(direction, x))
	z = normalize(direction)
	return geom.changeOfBasisMatrix(position, x,y,z)

def invertDirection(direction):
	return [-direction[0], -direction[1], -direction[2]]

def fillFeatureInput(featureInput, featureNode, min, max, prototype):
	position, direction, diameter = featureInput
	if int(diameter) >= min and int(diameter) <= max:
		direction = invertDirection(direction)
		inputNode = scene.createOccurrence('Input ' + str(diameter) + 'mm', featureNode)
		transform = computeMatrixFromTransform(position, direction)
		core.setProperty(inputNode, 'Transform', str(transform))
		instance = scene.prototypeSubTree(prototype)
		scene.setParent(instance, inputNode)
		core.setProperty(instance, 'Visible', 'Inherited')

def fillFeature(feature, featuresNode, min, max, prototype, count):
	featureType, inputs = feature
	
	name = information.featureTypeToName(featureType)
	featureNode = scene.createOccurrence(name, featuresNode)

	if len(inputs) == 2:
		fillFeatureInput(inputs[0], featureNode, min, max, prototype)	
		return count + 1
	for input in inputs:
		fillFeatureInput(input, featureNode, min, max, prototype)		
	return count + 1

def fillFeaturesWithOccurrence(features, min, max, prototype, count):
	occurrence, features = features
	featuresNode = scene.createOccurrence("Features", occurrence)
	# features returned by listFeatures are in world coordinates
	# apply the inverse of the global matrix to the featuresNode	
	matrix = scene.getGlobalMatrix(occurrence)
	matrix = geom.invertMatrix(matrix)
	core.setProperty(featuresNode, "Transform", str(matrix))
	
	for feature in features:
		count = fillFeature(feature, featuresNode, min, max, prototype, count)
	return count

def fillOccurrencesWithOccurrence(occurrences, min, max, prototype, throughHoles, blindHoles):
	featuresList = algo.listFeatures(occurrences, throughHoles, blindHoles, max)
	count = 0
	if len(featuresList) == 0:
		displayMessage('No features were found')
		return
	core.pushProgression(len(featuresList))
	for features in featuresList:
		core.stepProgression()
		count = fillFeaturesWithOccurrence(features, min, max, prototype, count)
	displayMessage('%d features filled!' % count)

def createPrimitiveOccurrence(primitiveOptions):
	primitive = scene.createOccurrence('primitive')
	part = scene.addComponent(primitive, scene.ComponentType.Part)
	sphere = cad.createSphereSurface(50)
	model = cad.createModel()
	scene.setPartModel(part, model)
	face = cad.createFace(sphere)
	cad.addToModel(face, model)
	algo.tessellate([primitive], -1, -1, -1)
	algo.replaceByPrimitive([primitive], primitiveOptions[0])
	scene.movePivotPointToOccurrenceCenter(primitive, True)
	scene.setParent(primitive, scene.getRoot())
	core.setProperty(primitive, 'Transform', str([[1,0,0,0], [0,1,0,0], [0,0,1,0], [0,0,0,1]]))
	scene.rotate(primitive, [1,0,0], 90)
	return primitive

def displayMessage(message):
	core.setInteractiveMode(True)
	core.message(message)
	core.setInteractiveMode(False)

def fillFeatures(occurrences, bounds, options, throughHoles, blindHoles):
	core.removeConsoleVerbose(2)
	min = bounds[0]
	max = bounds[1]
	option = options[0]
	if option == 'occurrence':
		prototype = options[1]
	elif option == 'primitive':
		prototype = createPrimitiveOccurrence(options[1])
	try:
		fillOccurrencesWithOccurrence(occurrences, min, max, prototype, throughHoles, blindHoles)
	except:
		pass
	core.addConsoleVerbose(2)
