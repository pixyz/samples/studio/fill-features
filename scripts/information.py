try:
	from pxz import algo
	from pxz import core
except:
	pass

def featureTypeToName(featureType):
	if featureType == algo.FeatureType.ThroughHole:
		return "Through Hole"
	elif featureType == algo.FeatureType.BlindHole:
		return "Blind Hole"
	else:
		return "Unknown"

def diametersToMessage(diameters):
	message = '['
	for diameter in diameters:
		message += str(diameter) + ', '
	message = message[:-2] + ']'
	return message

def getFeaturesInformation(occurrence, features, add_metadata):
	diameters = list()
	count = -1
	for feature in features:
		count += 1
		feature_type, inputs = feature
		for input in inputs:
			position, direction, diameter = input
			if add_metadata:
				core.addCustomProperty(occurrence, 'Feature type (%d)' % count, featureTypeToName(feature_type))
				core.addCustomProperty(occurrence, 'Feature position (%d)' % count, str(position))
				core.addCustomProperty(occurrence, 'Feature direction (%d)' % count, str(direction))
				core.addCustomProperty(occurrence, 'Feature diameter (%d)' % count, str(diameter))
			if diameter not in diameters: diameters.append(round(float(diameter), 2))
	return diameters

def featuresInformation(occurrences, through_hole, blind_hole, add_metadata):
	core.removeConsoleVerbose(2)
	occ_features_list = algo.listFeatures(occurrences, through_hole, blind_hole, -1)
	diameters = list()
	core.pushProgression(len(occ_features_list))
	for occ_features in occ_features_list:
		core.stepProgression()
		for diameter in getFeaturesInformation(occ_features[0], occ_features[1], add_metadata):
			if diameter not in diameters: diameters.append(diameter)
	diameter = diameters.sort()
	msg = diametersToMessage(diameters)
	core.setInteractiveMode(True)
	core.askString('Diameters', msg)
	core.setInteractiveMode(False)
	core.addConsoleVerbose(2)
	return diameters